# Compte rendu TP Colis

Dans ce TP, nous avons mis en place une gestion des colis
Nous pouvons :

Créer des agences,
- Créer un colis,
- Modifier un colis,
- Afficher un colis


Ce que l'on ne peut pas faire:

- Supprimer un colis


Pour tester le projet, il faut récupérer les sources sur le gitlab suivant: https://gitlab.com/Thriss/tp1-jee-talbot.git
Pour déployer le projet, il faut un serveur WildFly ou GlassFish, et déployer l'archie CompteEAR.ear sur un serveur
JavaEE 7
(Pour les serveurs GlassFish, penser à démarrer la BDD à coté).



Niveau arborescence:

Les objets EJB sont stockés dans l'archive CompteEjb
-> L'EJB session est stocké dans CompteEjb/src/main/java/fr.usmb.m2.isc.javaee.comptes/ejb/
-> Les objets JPA sont stockés dans CompteEjb/src/main/java/fr.usmb.m2.isc.javaee.comptes/jpa/


L'application est dans l'archive CompteWeb
-> Dans CompteWeb/src/main/java/fr.usmb.m2.isc.javaee.compte.web/ sont stockés les servlets
-> Dans CompteWeb/src/main/webapp sont stockés les pages html et jsp.