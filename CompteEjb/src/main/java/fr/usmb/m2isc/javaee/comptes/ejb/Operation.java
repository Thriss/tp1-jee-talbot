package fr.usmb.m2isc.javaee.comptes.ejb;

import fr.usmb.m2isc.javaee.comptes.jpa.Colis;

public interface Operation {

	Colis creerColis(float weight, float price, String origin, String destination, float longitude, float lattitude);

	Colis getColis(int id);

	void modifyLocationColis(int id, String emplacement, float longitude, float lattitude);

	void modifyStateColis(int id, String state);

}