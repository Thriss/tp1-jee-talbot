package fr.usmb.m2isc.javaee.comptes.ejb;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.usmb.m2isc.javaee.comptes.jpa.Colis;

@Stateless
@Remote
public class OperationBean implements Operation {
	
	@PersistenceContext
	private EntityManager em;
	
	public OperationBean() {
	}

	@Override
	public Colis creerColis(float weight, float price, String origin, String destination, float longitude, float lattitude) {
		Colis pck = new Colis(weight, price, origin, destination, longitude, lattitude);
		em.persist(pck);
		return pck;
	}
	
	@Override
	public Colis getColis(int id) {
		return em.find(Colis.class, id);
	}


	@Override
	public void modifyLocationColis(int id, String emplacement, float longitude, float lattitude) {
		Colis cl = getColis(id);
		cl.setEmplacement(emplacement);
		cl.setLongitude(longitude);
		cl.setLattitude(lattitude);
	}

	public void modifyStateColis(int id, String state) {
		Colis cl = getColis(id);
		cl.setState(state);
	}

}
