<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Info coli</title>
	<link rel="stylesheet" type="text/css" href="css/base.css" >
</head>
<body>
	<h1>Affichage coli</h1>
	<h2>Informations coli :</h2>
	<p>id : ${colis.id }</p>
	<p>localisation courrante : ${colis.emplacement }  </p>
	<p>Destination : ${colis.destination }  </p>
	<p>Etat : ${colis.state }  </p>
	<a href="index.html">Revenir � la page principale</a>
</body>
</html>