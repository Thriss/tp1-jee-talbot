package fr.usmb.m2isc.javaee.comptes.web;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.usmb.m2isc.javaee.comptes.ejb.Operation;
import fr.usmb.m2isc.javaee.comptes.jpa.Colis;

/**
 * Servlet utilisee pour creer un compte.
 */
@WebServlet("/CreerColisServlet")
public class CreerColisServlet extends HttpServlet{

	private static final long serialVersionUID = 5236668439173484090L;
	@EJB
	private Operation op;
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
	public CreerColisServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String weight = request.getParameter("poids");
		float poids = Float.parseFloat(weight);
		String priece = request.getParameter("prix");
		float prix = Float.parseFloat(priece);
		String origine = request.getParameter("origine");
		String destination = request.getParameter("destination");
		float longitude = Float.parseFloat(request.getParameter("longitude"));
		float lattitude = Float.parseFloat(request.getParameter("lattitude"));


		Colis col = op.creerColis(poids, prix, origine, destination, longitude, lattitude);


		request.setAttribute("colis", col);

		request.getRequestDispatcher("/AfficherColi.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
