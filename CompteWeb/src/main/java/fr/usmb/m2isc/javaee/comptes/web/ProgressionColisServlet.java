package fr.usmb.m2isc.javaee.comptes.web;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.usmb.m2isc.javaee.comptes.ejb.Operation;
import fr.usmb.m2isc.javaee.comptes.jpa.Colis;



/**
 * Servlet utilisee pour afficher un compte.
 */
@WebServlet("/ProgressionColisServlet")
public class ProgressionColisServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private Operation ejb;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProgressionColisServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("Id");
		String newEmplacement = request.getParameter("Emplacement");
		String newLongitude = request.getParameter("Longitude");
		String newLattitude = request.getParameter("Lattitude");
		String newState = request.getParameter("Etat");

		ejb.modifyLocationColis(Integer.parseInt(id), newEmplacement, Float.parseFloat(newLongitude), Float.parseFloat(newLattitude));

		ejb.modifyStateColis(Integer.parseInt(id), newState);
		
		Colis cl = ejb.getColis(Integer.parseInt(id));
		
		request.setAttribute("colis", cl);

		//response.getWriter().println("C'est fait");

		request.getRequestDispatcher("/AfficherColi.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
